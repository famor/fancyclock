#!/usr/bin/env python3
# Makes a font subset of all the fonts
# Extracts only numbers and colon, i.e. what's needed for a clock

# requires fontforge.org to be installed as a python extension, which is default on Linux

import fontforge
import os

path = "app/src/main/res/font"
for font in os.listdir("app/src/main/res/font"):
    if "numbers" in font:
        continue
    original = os.path.join(path, font)
    c = fontforge.open(original)
    c.selection.select(("ranges", None), "0", ":")
    c.copy()

    n = fontforge.font()
    n.selection.select(("ranges", None), "0", ":")
    n.paste()

    # Copy metadata
    n.fullname = c.fullname
    n.familyname = c.familyname
    n.fontname = c.fontname
    n.copyright = c.copyright
    n.version = c.version
    n.encoding = c.encoding
    n.os2_weight = c.os2_weight

#     os.remove(original)
    filename = font.lower().replace("-", "_").replace(".", "_numbers.")
    n.generate(os.path.join(path, filename))
