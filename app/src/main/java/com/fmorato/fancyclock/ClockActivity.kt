package com.fmorato.fancyclock

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
import android.content.res.Configuration
import android.os.Bundle
import android.text.TextPaint
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_SP
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.fmorato.fancyclock.databinding.ClockViewBinding
import kotlin.math.floor


class ClockActivity : AppCompatActivity() {
    private var mVisible: Boolean = false
    private lateinit var mSharedPreferences: SharedPreferences
    private lateinit var binding: ClockViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mSharedPreferences = getSharedPreferences(this::class.simpleName, Context.MODE_PRIVATE)

        binding = ClockViewBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.slider.addOnChangeListener { _: View, value: Float, fromUser: Boolean ->
            binding.clockLabel.setTextSize(COMPLEX_UNIT_SP, value / 100F * DEFAULT_CLOCK_FONT_SIZE)

            if (fromUser) {
                mSharedPreferences.edit().apply {
                    if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                        putFloat(SLIDER_POSITION_LANDSCAPE, value)
                    else
                        putFloat(SLIDER_POSITION_PORTRAIT, value)
                }.apply()
            }
        }
        binding.mainView.setOnClickListener { toggle() }
        binding.orientationButton.setOnCheckedChangeListener { _, isChecked ->
            requestedOrientation =
                if (isChecked) {
                    SCREEN_ORIENTATION_SENSOR_LANDSCAPE
                } else {
                    SCREEN_ORIENTATION_UNSPECIFIED
                }

            loadSliderPosition(resources.configuration)
            mSharedPreferences.edit().apply {
                putBoolean(ORIENTATION_LOCKED, isChecked)
            }.apply()
        }
    }

    override fun onResume() {
        super.onResume()
        mSharedPreferences.apply {
            binding.orientationButton.isChecked = getBoolean(ORIENTATION_LOCKED, false)
            when (getBoolean(CONTROLS_VISIBLE, true)) {
                true -> show()
                false -> hide()
            }
            loadSliderPosition(resources.configuration)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        loadSliderPosition(newConfig)
    }

    private fun loadSliderPosition(configuration: Configuration) {
        mSharedPreferences.apply {
            binding.slider.value = when (configuration.orientation) {
                Configuration.ORIENTATION_LANDSCAPE -> getFloat(
                    SLIDER_POSITION_LANDSCAPE,
                    DEFAULT_SLIDER_POSITION
                )
                Configuration.ORIENTATION_PORTRAIT -> getFloat(SLIDER_POSITION_PORTRAIT, 100F)
                else -> DEFAULT_SLIDER_POSITION
            }
        }

        val calcPaint = TextPaint(binding.clockLabel.paint)
        val scaledSizeInPixels: Float = TypedValue.applyDimension(
            COMPLEX_UNIT_SP,
            DEFAULT_CLOCK_FONT_SIZE,
            this.resources.displayMetrics
        )
        calcPaint.textSize = scaledSizeInPixels
        val maxSliderSize =
            floor(100F * resources.displayMetrics.widthPixels / calcPaint.measureText("00:00"))
        binding.slider.valueTo = maxSliderSize
    }

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
        mSharedPreferences.edit().apply {
            putBoolean(CONTROLS_VISIBLE, mVisible)
        }.apply()
    }

    //    https://stackoverflow.com/questions/62577645/android-view-view-systemuivisibility-deprecated-what-is-the-replacement
    private fun hide() {
        binding.slider.visibility = View.GONE
        binding.orientationButton.visibility = View.GONE
        mVisible = false
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, binding.root).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    private fun show() {
        binding.slider.visibility = View.VISIBLE
        binding.orientationButton.visibility = View.VISIBLE
        mVisible = true
        WindowCompat.setDecorFitsSystemWindows(window, true)
        WindowInsetsControllerCompat(
            window,
            binding.root
        ).show(WindowInsetsCompat.Type.systemBars())
    }

    companion object {
        const val SLIDER_POSITION_PORTRAIT = "pref_slider_position_portrait"
        const val SLIDER_POSITION_LANDSCAPE = "pref_slider_position_landscape"
        const val CONTROLS_VISIBLE = "pref_controls_visible"
        const val ORIENTATION_LOCKED = "pref_orientation_locked"
        const val DEFAULT_CLOCK_FONT_SIZE = 85F
        const val DEFAULT_SLIDER_POSITION = 100F
    }
}
